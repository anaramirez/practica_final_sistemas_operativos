build:
	mkdir bin	
	g++ src/ex1.cpp -o bin/ex1 -L/usr/local/lib -I/usr/local/include -lyaml-cpp
run:
	bin/ex1 examples/*
clean:
	rm bin/*
