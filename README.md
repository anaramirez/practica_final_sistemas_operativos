# README #

Realizado por:
Ana María Ramírez
Sara Castrillón

### Descripción General ###

este es un programa que recibe un archivo YAML
o parsea y forma estructuras de "Jobs" de acuerdo
a la descripción del YAML, formando así procesos
los cuales serán comunicados a través de tuberias

### Forma de uso ###

para ejecutar el programa simplemente es necesario
estar ubicado en la carpeta "practica_final_sistemas_operativos",
una vez ubicado allí, se escribe el comando "make build" para 
crear el directorio "bin" y poner ahí el archivo compilado,
luego de esto ejecutamos la instrucción "make run" para ejecutar
el archivo compilado anteriormente.
Por ultimo limpiamos ejecutando "make clean" para borrar el 
directorio bin.