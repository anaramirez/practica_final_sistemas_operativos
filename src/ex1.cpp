#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include "yaml-cpp/yaml.h"
#include <map>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <vector>

using namespace std;

int main(int argc, char* argv[]) {
//argv es un arreglo q tiene los parametros con los q se llama el programa, la posicion 0 esta vaci siempre
//YAML::Node node = YAML::LoadFile(argv[1]);

//YAML::Node node_trabajo1 = node["Jobs"];
//cout << node["Jobs"] << "\n";


//cout << names1 << "\n";

  YAML::Node node = YAML::LoadFile(argv[1]);
  int numberOfJobs = node["Jobs"].size();
  int numberOfPipes = node["Pipes"].size();

  //cout << sizeProcesses << endl;
  //cout << sizePipes << endl;

  //string names1 = node["Jobs"][0]["Name"].as<string>();
  //cout << names1 << endl;
  
  map<string, int> positionsJobs;
  string names[numberOfJobs];
  string exec[numberOfJobs];
  int numbersOfArgs[numberOfJobs];
  string* args [numberOfJobs];

  //Mètodo para parsear los Jobs
  for (int i=0; i<numberOfJobs; i++){ 
    
    names[i] = node["Jobs"][i]["Name"].as<string>();
    positionsJobs[names[i]] = i;
    exec[i] = node["Jobs"][i]["Exec"].as<string>();
  	args[i] = new string[node["Jobs"][i]["Args"].size()];
    numbersOfArgs[i] = node["Jobs"][i]["Args"].size();
    for(int j=0; j<numbersOfArgs[i]; j++){
      args[i][j] = node["Jobs"][i]["Args"][j].as<string>();
    }  
   
  }
  
  map<string, int> positionsPipes;
  string namesPipes[numberOfPipes];
  string input[numberOfPipes];
  string output[numberOfPipes];
  int numbersOfPipes[numberOfPipes];
  int* argsPosition[numberOfPipes];
  
  //Método para parsear las tuberías
  for (int i=0; i<numberOfPipes; i++){ 
    
  	namesPipes[i] = node["Pipes"][i]["Name"].as<string>();
    positionsPipes[namesPipes[i]] = i;
    input[i] = node["Pipes"][i]["input"].as<string>();
    output[i] = node["Pipes"][i]["output"].as<string>();
  	argsPosition[i] = new int[node["Pipes"][i]["Pipe"].size()];
    //cout << "Sara" << endl;
    numbersOfPipes[i] = node["Pipes"][i]["Pipe"].size();
    for(int j=0; j<numbersOfPipes[i]; j++){
      argsPosition[i][j] = positionsJobs[node["Pipes"][i]["Pipe"][j].as<string>()];
    }  
   
  }
/*
	string job0[sizeof (numbersOfArgs) + 2];
  string job1[4];
  job0[0] = exec[0];
  job1[0] = exec[1];
  for(int i=1; i< numbersOfArgs[0]; i++){
   job0[i] = args[0][i];
  }
  for(int i=0; i<job0.size(); i++){
   cout << job0[1][i] << endl;
  }
  
  
  cout << job0[0] << endl;
  cout << job1[0] << endl;

  cout << names[2] << endl;
  cout << exec[2] << endl;

  cout << namesPipes[0] << endl;
  cout << input[0] << endl;
  cout << output[0] << endl;

  
  cout << positionsJobs["job2"] << endl;
  for(int i=0; i< numbersOfArgs[2]; i++){
    cout << args[2][i] << endl;
  }

  for(int i=0; i< numbersOfPipes[0]; i++){
    cout << argsPosition[0][i] << endl;
  }*/

  
  int filed0[2];
  int filed1[2];
  int filed2[2];
  int filed3[2];
  int pid;
  
  //Instrucciones para castear los objetos
  string ejer = exec[0];
  //cout << ejer << endl;
  const char * c = ejer.c_str();
  //cout << c << endl;
  char * exec1;
  exec1 = const_cast<char *>(c);
  //cout << exec1 << endl;
  
  string temp;
  for(int i=0; i< numbersOfArgs[2]; i++){
    //cout << args[0][i] << endl;
    temp =temp + " " +args[0][i];
  }
  const char * b = temp.c_str();
  char * args1;
  args1 = const_cast<char *>(b);

  //cout << temp << endl;
  char *job_1[] = {exec1,args1, NULL};
  cout << "El exec del Job1 es: " << job_1[0] << endl;
  cout << "Los argumentos del Job1 son: " << job_1[1] << endl;

  string ejer2 = exec[1];
  //cout << ejer << endl;
  const char * d = ejer2.c_str();
  //cout << c << endl;
  char * exec2;
  exec2 = const_cast<char *>(d);
  //cout << exec1 << endl;

  string temporal;
  for(int i=0; i< numbersOfArgs[2]; i++){
    //cout << args[0][i] << endl;
    temporal =temporal+ " " +args[2][i];
  }
  
  //cout << temporal<< endl;
  const char * e = temporal.c_str();
  char * args2;
  args2 = const_cast<char *>(e);
  char *job_2[] = {exec2,args2, "nunca", NULL};
  cout << "El exec del Job2 es: " << job_2[0] << endl;
  cout << "Los argumentos del Job2 son: " << job_2[1] << endl;

  
  vector<int*> filedes;
  // make a pipe (fds go in pipefd[0] and pipefd[1])
  pipe(filed0);
  pipe(filed1);
 
  filedes.push_back(filed0);
  filedes.push_back(filed1);
 
  //se crea un vector con cada uno de los trabajos
  vector<char **> executes;
  executes.push_back(job_1);
  executes.push_back(job_2);
 

  //cout << "El fd 0 es: " << fd0[0] << " " << fd0[1] << endl;
  //cout << "El fd 1 es: " << fd1[0] << " " << fd1[1] << endl;

  for(int i = 0; i < filedes.size(); ++i) printf("El vector fd %d es: %d %d\n", i, filedes[i][0], filedes[i][1]);
  //cout << "El exe 0 es: " << job_1[0] << " " << job_1[1] << endl;
  //	cout << "El exe 1 es: " << job_2[0] << " " << job_2[1] << endl;

  for(int i = 0; i < executes.size() - 1; ++i) printf("El otro exe %d es: %s %s\n", i, executes[i][0], executes[i][1]);
  //cout << "El otro exe es: " << exes[exes.size() - 1][0] << endl;
  for(int i = 0; i < executes.size(); ++i){
    pid = fork();
    if(pid == 0){
      if(i > 0){
        close(0);
        dup(filedes[i - 1][0]);
        close(filedes[i - 1][0]);
        close(filedes[i - 1][1]);
      }
      if(i < executes.size() - 1){
        close(1);
        dup(filedes[i][1]);
        close(filedes[i][0]);
        close(filedes[i][1]);
      }
      execvp(executes[i][0], executes[i]);
    }else{
      if(i > 0){
        close(filedes[i - 1][0]);
        close(filedes[i - 1][1]);
      }
    }
  }
  int status;
	while (wait(&status) != -1);

  return 0;
}
    

